package com.example.a7.springdatajpa.repository;

import com.example.a7.springdatajpa.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "http://localhost:3000")
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    List<Doctor> findAllByNameContains(String name);
}

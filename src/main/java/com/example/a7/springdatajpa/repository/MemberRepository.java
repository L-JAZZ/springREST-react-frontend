package com.example.a7.springdatajpa.repository;

import com.example.a7.springdatajpa.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "http://localhost:3000")
public interface MemberRepository extends JpaRepository<Member, Long> {
    Member findByUsername(String memberName);
}

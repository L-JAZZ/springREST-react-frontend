package com.example.a7.springdatajpa.controllers;

import com.example.a7.springdatajpa.exceptions.ExceptionService;
import com.example.a7.springdatajpa.exceptions.GlobalExceptionHandler;
import com.example.a7.springdatajpa.model.Medicine;
import com.example.a7.springdatajpa.model.Member;
import com.example.a7.springdatajpa.repository.MemberRepository;
import com.example.a7.springdatajpa.service.UserService;
import com.example.a7.springdatajpa.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "http://localhost:3000")
public class MemberController {
    @Autowired
    MemberRepository memberRepository;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    private GlobalExceptionHandler exceptionService;

    @GetMapping
    public ResponseEntity<?> testException() {
        return ResponseEntity.ok(exceptionService.handleAnyException(new Exception()));
    }

    @GetMapping("/all")
    public List<Member> getMemberList(){
        return memberRepository.findAll();
    }

    @GetMapping("{id}")
    public Member getById(@PathVariable Long id){
        return memberRepository.findById(id).get();
    }

    @GetMapping("/create/")
    public void createMemberByUsernamePassword(String username, String password,String phone){
        Member member = new Member();
        member.setUsername(username);
        member.setPassword(password);
        member.setPhoneNum(phone);
        userService.createMember(member);
    }

    @PostMapping("/add")
    public void addMember(@RequestBody Member member){
        memberRepository.save(member);
    }

    @PutMapping("/{id}")
    public void updateMember(@PathVariable Long id,
                             @RequestBody Member member){
        System.out.println("MemberController.updateUser");
        System.out.println("ID = " + id);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getName();

        userService.updateMember(id,member);

    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Member> updateMedicine(@RequestBody Member member){
        return new ResponseEntity<>(memberRepository.saveAndFlush(member), HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public void removeMember(@PathVariable Long id){
        Member member = memberRepository.findById(id).get();
        memberRepository.delete(member);
    }

    public MemberController(MemberRepository memberRepository){
        this.memberRepository = memberRepository;
    }
}

package com.example.a7.springdatajpa.controllers;
/*
{
        "quantity": "5",
        "title": "How to be a millionaire",
        "year": "2016",
}
*/

import com.example.a7.springdatajpa.exceptions.GlobalExceptionHandler;
import com.example.a7.springdatajpa.model.Medicine;
import com.example.a7.springdatajpa.repository.MedicineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicine")
@CrossOrigin(origins = "http://localhost:3000")
public class MedicineController {
    @Autowired
    private MedicineRepository medicineRepository;

    @Autowired
    private GlobalExceptionHandler exceptionService;

    @GetMapping
    public ResponseEntity<?> testException() {
        return ResponseEntity.ok(exceptionService.handleAnyException(new Exception()));
    }

    @PostMapping("/add")
    public void addMedicine(@RequestBody Medicine medicine){
        medicineRepository.save(medicine);
    }

    @GetMapping("/find/")
    public List<Medicine> getMedicineByTitle(@RequestParam("title") String title){
        return  medicineRepository.getBookByTitle(title);
    }

    @GetMapping("/available")
    public String availableMedicineList(){
        return  medicineRepository.findAvailableBook().toString();
    }

    @GetMapping("/all")
    public List<Medicine> getAllMedicine(){
        return medicineRepository.findAll();
    }

    @GetMapping("/{id}")
    public Medicine getMedicineById(@PathVariable Long id){
        return medicineRepository.findById(id).get();
    }

    @PutMapping("/rent/{id}")
    public void rentMedicine(@PathVariable Long id){
        Medicine medicine = medicineRepository.findById(id).get();
        medicine.buyMedicine();
        medicineRepository.saveAndFlush(medicine);
    }



    @CrossOrigin(origins = "http://localhost:3000")
/*    @PutMapping("/{id}")
    public void updateMedicine(@PathVariable Long id){
        Medicine medicine = medicineRepository.findById(id).get();
        medicineRepository.saveAndFlush(medicine);
    }*/

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Medicine> updateMedicine(@RequestBody Medicine medicine){
        return new ResponseEntity<>(medicineRepository.saveAndFlush(medicine), HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public void deleteMedicine(@PathVariable Long id){
        Medicine medicine = medicineRepository.findById(id).get();
        medicineRepository.delete(medicine);
    }

    public MedicineController(MedicineRepository medicineRepository){
        this.medicineRepository = medicineRepository;
    }
}

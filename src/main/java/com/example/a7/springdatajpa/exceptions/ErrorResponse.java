package com.example.a7.springdatajpa.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.bind.annotation.CrossOrigin;

@Data
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class ErrorResponse {
    private String code;
    private String message;
}

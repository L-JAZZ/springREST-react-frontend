package com.example.a7.springdatajpa.exceptions;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

@Service
@CrossOrigin(origins = "http://localhost:3000")
public class ExceptionService {

    public int testException() {
        throw new NullPointerException("Some exception");
    }
}

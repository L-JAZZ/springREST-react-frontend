import React from "react";
import {Card, Table} from "react-bootstrap";

import axios from 'axios';
import {Link} from "react-router-dom";
import ButtonGroup from "react-bootstrap/ButtonGroup";

class MembersList extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            member:[]
        };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/users/all')
            .then(response=>response.data)
            .then((data)=>{
                this.setState({member:data});
            });
    }

    deleteMember(id){
        axios.delete("http://localhost:8080/users/"+id)
            .then(response=> {
                    if (response.data != null) {
                        alert("Deleting member with id:" + id);
                        this.setState({
                            member: this.state.member.filter(member=>member.member_id !== id)
                        })
                    }
                }
            )
    };

    render() {
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header >
                    Member List
                </Card.Header>
                <Card.Body className={"bg-white"}>
                    <Table striped bordered hover >
                        <thead>
                            <tr>
                                <th>UserID</th>
                                <th>Username</th>
                                <th>phone number</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.member.map((member)=>(
                                <tr key={member.member_id}>
                                    <td>{member.member_id}</td>
                                    <td>{member.username}</td>
                                    <td>{member.phoneNum}</td>
                                    <ButtonGroup>
                                        <div className={"btn btn-sm btn-online-primary"}
                                             onClick={this.deleteMember.bind(this, member.member_id)} >
                                            Delete
                                        </div>
                                        <Link to={"edit/"+member.member_id} className={"btn btn-sm btn-online-primary"}>  Edit</Link>
                                    </ButtonGroup>
                                </tr>
                            ))
                        }
                        </tbody>

                    </Table>
                </Card.Body>
            </Card>
        );
    }
}

export default MembersList
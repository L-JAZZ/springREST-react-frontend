import React from "react";
import {Navbar,Nav} from "react-bootstrap";
import {Link} from "react-router-dom";

class NavigationBar extends React.Component{
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                {/*<Link to={"/"} className={"navbar-brand"}>Welcome</Link>*/}
                <Navbar.Brand href="/">Drug store</Navbar.Brand>
                <Nav className="mr-auto">
                    <Link to={"/members"} className={"nav-link"}>Members</Link>
                    <Link to={"/membersList"} className={"nav-link"}>All Members</Link>
                    <Link to={"/medicine"} className={"nav-link"}>Drugs</Link>
                    <Link to={"/medicineList"} className={"nav-link"}>All Drugs</Link>
                    <Link to={"/doctors"} className={"nav-link"}>Doctors</Link>
                    <Link to={"/doctorsList"} className={"nav-link"}>All Doctors</Link>
                </Nav>
            </Navbar>
        );
    }
}

export default NavigationBar;
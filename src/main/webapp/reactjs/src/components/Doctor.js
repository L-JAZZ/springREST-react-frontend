import React from "react";
import {Card, Col} from "react-bootstrap";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from 'axios'


class Doctor extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            name:'',
            surname:'',
        };

        this.onDoctorChange = this.onDoctorChange.bind(this);
        this.submitDoctor = this.submitDoctor.bind(this);
    }

    submitDoctor = event => {

        event.preventDefault();

        const DocConstant={
            name: this.state.name,
            surname:  this.state.surname
        };

        axios.post("http://localhost:8080/doctors/add",DocConstant)
            .then(response=>{
                if(response.data != null){
                    alert("new Doctor added!");
                }
            });
    };

    updateDoctor = event => {///////////////////////////////////////////////  update
        event.preventDefault();

        const DocConstant={
            id:this.state.id,
            name: this.state.name,
            surname:  this.state.surname
        };

        axios.put("http://localhost:8080/doctors/",DocConstant)
            .then(response=>{
                if(response.data != null){
                    alert("Doctor updated!");
                }else
                    alert("Chet ne poluchilos");
            });
    };

    onDoctorChange = event => {
        this.setState({
            [event.target.name]:event.target.value
        })
    };

    doctorList = () => {
        return this.props.history.push("/doctorsList")
    };

    componentDidMount() {
        const id = +this.props.match.params.id;
        if(id){
            this.findDoctorById(id);
        }
    }

    findDoctorById=(docid)=>{
        axios.get("http://localhost:8080/doctors/"+docid)
            .then(response=>{
                if(response!=null){
                    this.setState({
                        id: response.data.id,
                        name: response.data.name,
                        surname: response.data.surname
                    });
                }
            }).catch((error)=>{
            console.error("Error-"+error)
        })
    };


    render() {
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header >
                    Add Doctor
                </Card.Header>
                <Form onSubmit={this.state.id ? this.updateDoctor : this.submitDoctor}>
                    <Card.Body className={"bg-white"}>
                        <Form.Row>
                            <Form.Group as={Col} controlId={"R_name"}>
                                <Form.Label>Doctor name</Form.Label>
                                <Form.Control required
                                              onChange={this.onDoctorChange}
                                              value={this.state.name}
                                              name={"name"}
                                              placeholder="Enter doctor name"/>
                            </Form.Group>
                            <Form.Group as={Col} controlId={"R_surname"}>
                                <Form.Label>Doctor Surname</Form.Label>
                                <Form.Control required
                                              value={this.state.surname}
                                              onChange={this.onDoctorChange}
                                              name={"surname"}
                                              placeholder="Enter doctor surname" />
                            </Form.Group>
                        </Form.Row>
                    </Card.Body>
                    <Card.Footer>
                        <Button variant="success" size={"sm"} type="submit">
                            {this.state.id ? "Update" : "Add"}
                        </Button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <Button variant="info" size={"sm"} type="button" onClick={this.doctorList.bind()}>
                            List
                        </Button>
                    </Card.Footer>
                </Form>
            </Card>

        );
    }
}

export default Doctor
import React from "react";
import {Card, Col} from "react-bootstrap";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from 'axios'


class Medicine extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            quantity:'',
            title:'',
            year:'',
        };


        this.onMedicineChange = this.onMedicineChange.bind(this);
        this.submitMedicine = this.submitMedicine.bind(this);
    }

    submitMedicine = event => {

        event.preventDefault();

        const DrugConstant={
            title: this.state.title,
            quantity:  this.state.quantity,
            year:  this.state.year
        };

        axios.post("http://localhost:8080/medicine/add",DrugConstant)
            .then(response=>{
                if(response.data != null){
                        alert("new Medicine added!");
                }
            });
    };

    updateMedicine = event => {///////////////////////////////////////////////  update
        event.preventDefault();
        const DrugConstant={
            medicine_id:this.state.medicine_id,
            title: this.state.title,
            quantity:  this.state.quantity,
            year:  this.state.year
        };

        axios.put("http://localhost:8080/medicine/",DrugConstant)
            .then(response=>{
                if(response.data != null){
                    alert("Medicine updated!");
                }else alert("Chet ne poluchilos");
            });
    };

    onMedicineChange = event => {
        this.setState({
            [event.target.name]:event.target.value
        })
    };

    memberList = () => {
        return this.props.history.push("/medicineList")
    };

    componentDidMount() {
        const medicineid = +this.props.match.params.medicine_id;
        if(medicineid){
            this.findMedicineById(medicineid);
        }
    }

    findMedicineById=(medicineid)=>{
        axios.get("http://localhost:8080/medicine/"+medicineid)
            .then(response=>{
                if(response!=null){
                    this.setState({
                        medicine_id: response.data.medicine_id,
                        title: response.data.title,
                        quantity: response.data.quantity,
                        year:response.data.year
                    });
                }
            }).catch((error)=>{
            console.error("Error-"+error)
        })
    };


    render() {
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                    <Card.Header >
                        Add Medicine
                    </Card.Header>
                <Form onSubmit={this.state.medicine_id ? this.updateMedicine : this.submitMedicine}>
                    <Card.Body className={"bg-white"}>
                        <Form.Row>
                            <Form.Group as={Col} controlId={"R_title"}>
                                <Form.Label>User name</Form.Label>
                                <Form.Control required
                                  onChange={this.onMedicineChange}
                                  value={this.state.title}
                                  name={"title"}
                                  placeholder="Enter drug name"/>
                            </Form.Group>
                            <Form.Group as={Col} controlId={"R_quantity"}>
                                <Form.Label>User phone number</Form.Label>
                                <Form.Control required
                                    value={this.state.quantity}
                                    onChange={this.onMedicineChange}
                                    name={"quantity"}
                                    placeholder="Enter quantity" />
                            </Form.Group>
                            <Form.Group as={Col} controlId={"R_year"}>
                                <Form.Label>Year</Form.Label>
                                <Form.Control
                                      value={this.state.year}
                                      name={"year"}
                                      onChange={this.onMedicineChange}
                                      placeholder="year" />
                            </Form.Group>
                        </Form.Row>
                    </Card.Body>
                    <Card.Footer>
                        <Button variant="success" size={"sm"} type="submit">
                            {this.state.medicine_id ? "Update" : "Add"}
                        </Button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <Button variant="info" size={"sm"} type="button" onClick={this.memberList.bind()}>
                            List
                        </Button>
                    </Card.Footer>
                </Form>
            </Card>

        );
    }
}

export default Medicine
import React from "react";
import {Card, Table} from "react-bootstrap";

import axios from 'axios';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {Link} from "react-router-dom";

class MedicineList extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            medicine:[]
        };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/medicine/all')
            .then(response=>response.data)
            .then((data)=>{
                this.setState({medicine:data});
            });
    }

    deleteMedicine(id){
      axios.delete("http://localhost:8080/medicine/"+id)
          .then(response=> {
                  if (response.data != null) {
                      alert("Deleting medicine with id:" + id);
                      this.setState({
                          medicine: this.state.medicine.filter(medicine=>medicine.medicine_id !== id)
                      })
                  }
              }
          )
    };

    render() {
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header >
                    Medicine List
                </Card.Header>
                <Card.Body className={"bg-white"}>
                    <Table striped bordered hover >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Quantity</th>
                            <th>Year</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.medicine.map((medicine)=>(
                                <tr key={medicine.medicine_id}>
                                    <td>{medicine.medicine_id}</td>
                                    <td>{medicine.title}</td>
                                    <td>{medicine.quantity}</td>
                                    <td>{medicine.year}</td>
                                    <td>
                                        <ButtonGroup>
                                            <div className={"btn btn-sm btn-online-primary"}
                                               onClick={this.deleteMedicine.bind(this, medicine.medicine_id)} >
                                                Delete
                                            </div>
                                            <Link to={"edit/"+medicine.medicine_id} className={"btn btn-sm btn-online-primary"}>  Edit</Link>
                                        </ButtonGroup>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>

                    </Table>
                </Card.Body>
            </Card>
        );
    }
}

export default MedicineList
import React from "react";
import {Card, Table} from "react-bootstrap";

import axios from 'axios';
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {Link} from "react-router-dom";

class DoctorsList extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            doctor:[]
        };
    }

    componentDidMount() {
        axios.get('http://localhost:8080/doctors/all')
            .then(response=>response.data)
            .then((data)=>{
                this.setState({doctor:data});
            });
    }

    deleteDoctor(id){
        axios.delete("http://localhost:8080/doctors/"+id)
            .then(response=> {
                    if (response.data != null) {
                        alert("Deleting doctor with id:" + id);
                        this.setState({
                            doctor: this.state.doctor.filter(doctor=>doctor.id !== id)
                        })
                    }
                }
            )
    };

    render() {
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header>
                    Doctors List
                </Card.Header>
                <Card.Body className={"bg-white"}>
                    <Table striped bordered hover >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Surname</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.doctor.map((doctor)=>(
                                <tr key={doctor.id}>
                                    <td>{doctor.id}</td>
                                    <td>{doctor.name}</td>
                                    <td>{doctor.surname}</td>
                                    <td>
                                        <ButtonGroup>
                                            <div className={"btn btn-sm btn-online-primary"}
                                                 onClick={this.deleteDoctor.bind(this, doctor.id)} >
                                                Delete
                                            </div>
                                            <Link to={"editDoctor/"+doctor.id} className={"btn btn-sm btn-online-primary"}>  Edit</Link>
                                        </ButtonGroup>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>

                    </Table>
                </Card.Body>
            </Card>
        );
    }
}

export default DoctorsList
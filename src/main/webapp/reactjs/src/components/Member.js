import React from "react";
import {Card, Col} from "react-bootstrap";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";


class Member extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            username:'',
            phoneNum:'',
            password:''
        };
        this.onMemberChange = this.onMemberChange.bind(this);
        this.submitMember = this.submitMember.bind(this)
    }



    submitMember = event => {
        event.preventDefault();

        const UsersConstant={
            username: this.state.username,
            phoneNum:  this.state.phoneNum,
            password:  this.state.password
        };

        axios.post("http://localhost:8080/users/add",UsersConstant)
            .then(response=>{
                if(response.data != null){
                    alert("new User added!");
                }
            });
    };

    updateMember = event => {///////////////////////////////////////////////  update
        event.preventDefault();
        const DrugConstant={
            member_id:this.state.member_id,
            username: this.state.username,
            phoneNum:  this.state.phoneNum,
            password: this.state.password
        };

        axios.put("http://localhost:8080/users/",DrugConstant)
            .then(response=>{
                if(response.data != null){
                    alert("User updated!");
                }else alert("Chet ne poluchilos");
            });
    };


    onMemberChange(event){
        this.setState({
            [event.target.name]:event.target.value
        })
    }

    componentDidMount() {
        const user_id = +this.props.match.params.member_id;
        if(user_id){
            this.findUserById(user_id);
        }
    }

    findUserById=(user_id)=>{
        axios.get("http://localhost:8080/users/"+user_id)
            .then(response=>{
                if(response!=null){
                    this.setState({
                        member_id: response.data.member_id,
                        username: response.data.username,
                        phoneNum: response.data.phoneNum,
                        password: response.data.password
                    });
                }
            }).catch((error)=>{
            console.error("Error-"+error)
        })
    };

    memberList = () => {
        return this.props.history.push("/membersList")
    };

    render() {
        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Form onSubmit={this.state.member_id ? this.updateMember : this.submitMember} >
                    <Card.Header >
                        Add Members
                    </Card.Header>
                    <Card.Body className={"bg-white"}>
                    <Form.Row>
                        <Form.Group as={Col} controlId={"R_username"}>
                            <Form.Label>User name</Form.Label>
                            <Form.Control required
                                onChange={this.onMemberChange}
                                value={this.state.username}
                                name={"username"}
                                placeholder="Enter user name"
                            />
                        </Form.Group>
                        <Form.Group as={Col} controlId={"R_phone_num"}>
                            <Form.Label>User phone number</Form.Label>
                            <Form.Control required
                                value={this.state.phoneNum}
                                onChange={this.onMemberChange}
                                name={"phoneNum"}
                                placeholder="Enter phone number" />
                        </Form.Group>
                        <Form.Group as={Col} controlId={"R_password"}>
                            <Form.Label>Password</Form.Label>
                            <Form.Control required
                                          type="password"
                                          name={"password"}
                                          value={this.state.password}
                                          onChange={this.onMemberChange}
                                          placeholder="Password" />
                        </Form.Group>
                    </Form.Row>
                    </Card.Body>
                    <Card.Footer>
                    <Button variant="success" size={"sm"} type="submit">
                        {this.state.member_id ? "Update" : "Add"}
                    </Button>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <Button variant="info" size={"sm"} type="button" onClick={this.memberList.bind()}>
                        List
                    </Button>
                    </Card.Footer>
                </Form>
            </Card>

        );
    }
}

export default Member
import React from 'react';

import './App.css';
import {Col, Container, Row} from "react-bootstrap";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";


import NavigationBar from "./components/NavigationBar.js";
import Welcome from "./components/Welcome";
import Footer from "./components/Footer";
import Member from "./components/Member";
import MembersList from "./components/MembersList";
import Medicine from "./components/Medicine";
import MedicineList from "./components/MedicineList";
import Doctor from "./components/Doctor";
import DoctorsList from "./components/DoctorsList";


function App() {
    const marginTop ={
        marginTop:"20px"
    };

  return (
    <Router>
        <NavigationBar/>
       <Container>
           <Row>
               <Col lg={12} style={marginTop}>
                   <Switch>
                       <Route  path={"/"} exact component={Welcome}/>

                       <Route  path={"/members"} exact component={Member}/>
                       <Route  path={"/edit/:member_id"} exact component={Member}/>
                       <Route  path={"/membersList"} exact component={MembersList}/>

                       <Route  path={"/medicine"} exact component={Medicine}/>
                       <Route  path={"/edit/:medicine_id"} component={Medicine}/>
                       <Route  path={"/medicineList"} exact component={MedicineList}/>

                       <Route  path={"/doctors"} exact component={Doctor}/>
                       <Route  path={"/editDoctor/:id"} exact component={Doctor}/>
                       <Route  path={"/doctorsList"} exact component={DoctorsList}/>



                   </Switch>
               </Col>
           </Row>
       </Container>

        <Footer/>
    </Router>
  );
}

export default App;
